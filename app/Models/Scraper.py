import requests
from datetime import date, datetime
from bs4 import BeautifulSoup
import time
import pandas as pd
from requests_futures.sessions import FuturesSession


class Scraper():

    defaultUrl = "https://www.ecb.europa.eu/stats/" +\
        "policy_and_exchange_rates/euro_reference_exchange_rates" +\
        "/html/index.en.html"
    lastUpdate = date(1970, 1, 1)
    currentReferenceRates = dict()

    def __init__(self, database):
        self.lastUpdate = date(1970, 1, 1)
        self.session = FuturesSession()
        self.currentReferenceRates = ''
        self.database = database

    def getCurrentReferenceRates(self):
        future = self.session.get(self.defaultUrl)
        res = future.result()
        soup = BeautifulSoup(res.text, "html.parser")
        table = soup.find("table")
        rows = table.find_all("tr")[1:]
        dico = {}
        for row in rows:
            element = row.find_all("td")
            dico[element[0].text] = float(element[2].text)
        return dico

    def readWithPanda(self):
        df = pd.read_html(self.defaultUrl)
        return df

    def readWithPandaAndSaveToDb(self):
        df = self.readWithPanda(self.defaultUrl)
        self.database.saveDataframe(df)
