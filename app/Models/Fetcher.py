import requests
from datetime import datetime, date
import json

# CurrencyFetcher.py


class CurrencyFetcher():
    """
    A class used to get information for the frankfurter API

    Attributes
    ----------
    host: str

    Methods
    -------
    open()
        Open the database connection
    getOneCurrencyValue()
            Test the connection with the API, return a value
    getCurrencies()
            Return the values and the abbreviation
    getCurrencyName(currencyCode)
            Transform an abbreviation into the name of the currency
    getATimeSerie(currencyCode, beginPeriod, endPeriod)
            Get and return all the values of a currency from begin to endPeriod
    currencyPeriodYear(currencyCode, date)
            Return a time serie of the value of a currency from a year ago

    """

    defaultHost = 'api.frankfurter.app'

    def __init__(self, host=defaultHost):
        self.host = host

    def getOneCurrencyValue(self, curr, date=datetime.now()):
        """
        Test the connection with the API, return a value
            ...

        Returns
        -------
        string
            The get response of the API
        """
        res = requests.get(
            'https://{}/latest?amount=10&from=GBP&to=USD'.format(self.host))
        return res

    def getCurrencies(self):
        currencies = requests.get(
            'https://{host}/currencies'.format(host=self.host)).json()  # https://api.frankfurter.app/currencies
        return currencies

    def getCurrencies(self):
        """
        Return the values and the abbreviation
                ...

        Returns
        -------
        json
            The different currencies and their abbreviation
        """
        url = "https://api.frankfurter.app/currencies"
        # transform the text in dictionnary
        currencies = requests.get(url).json()
        return currencies

    def getCurrencyName(self, currencyCode):
        """
        Transform an abbreviation into the name of the currency
                ...

        Returns
        -------
        string
            The name of the currency
        """
        currencies = self.getCurrencies()
        return currencies[currencyCode]

    def getATimeSerie(self, currencyCode, beginPeriod, endPeriod=date.today()):
        """
        Test the connection with the API, return a value
                ...
        Parameters
        -------
        currencyCode: str
                The code of the currency we want the time serie of
        beginPeriod: date
                The time of the begin of the period of the time serie
        endPeriod: date
                The time of the end of the period of the time serie

        Returns
        -------
        json
            The time serie in the form of a json
        """
        params = {'to': currencyCode}
        url = 'https://{host}/{beginDate}..'.format(
            host=self.host, beginDate=beginPeriod.strftime('%Y-%m-%d'))  # Transformer la date en string
        # Format de l'url
        # Documentation différents moyen de récupérer
        # Une requête par jour paér Currency
        # Une requête pour tout
        text_rates = requests.get(url, params=params)
        rates = text_rates.json()
        # Transformation
        return rates  # dictionnaire

    def currencyPeriodYear(self, currencyCode, date=datetime.today()):
        """
        Return a time serie of the value of a currency from a year ago
        ...
        Parameters
        -------
        currencyCode: str
            The code of the currency we want the time serie of
        date: date
            The end of the period of one year the time serie

        Returns
        -------
        json
            The time serie in the form of a json
        """
        return self.getATimeSerie(currencyCode, datetime(year=date.year-1, month=date.month, day=date.day))
