# First we import all the librabries needed
from flask import Flask
from .Models.Fetcher import CurrencyFetcher
from .Models.Database import Database
from .Models.Forecaster import Forecaster
from .Models.Scraper import Scraper
from flask import render_template
import json
import os

# Then we create our flask app
app = Flask(__name__)

# If the database does not already exists, we create a new one
# and we fetch all the data from the frankfurter api to put them
# in the database
if not os.path.exists("Currencies.db"):
    database = Database("Currencies")
    fetcher = CurrencyFetcher()
    currencies = fetcher.getCurrencies()
    for currency in currencies.keys():
        if (currency == "EUR"):
            continue
        database.saveJSON(fetcher.currencyPeriodYear(currency), currency)
    database.close

# If the url is followed by nothing, the app return the main view with the
# explanation of our project.


@app.route('/')
def index():
    return render_template("Assignement2.html")

# Endpoint for the verification for Atrigger.com


@app.route('/ATriggerVerify.txt')
def verify():
    return "B80858ACEF821070326ED28FCD1A4DE3\n\
\n\This is ATrigger.com API Verification File.\
This file should be placed on the root folder of target url. This file is unique for each account in ATrigger.com\n\
http://example.com/mySite/Task?name=joe        This file should be available at: http://example.com/ATriggerVerify.txt\n\
http://sub.example.com/mySite/Task?name=joe    This file should be available at: http://sub.example.com/ATriggerVerify.txt\n\
"

# Endpoint used everyday by ATrigger.com to update the database
# This return the actual forecast of tomorrow


@app.route('/api/forecast')
def forecast():
    database = Database("Currencies")
    fetcher = CurrencyFetcher()
    scraper = Scraper(database)
    rates = scraper.getCurrentReferenceRates()
    forecaster = Forecaster(database)
    forecasts = dict()
    for currency in fetcher.getCurrencies():
        if (currency == "EUR"):
            continue
        database.insertTodayCurrency(currency, rates[currency])
        forecasts[currency] = forecaster.forecast(currency)

    return {"forecasted values": forecasts}
